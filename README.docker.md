# cleanbuild docker engine

With docker engine there's nothing needed to configure on local system other
than access to running docker engine and docker client program.

## Maintenance

To kill all containers created by cleanbuild:

```shell
docker ps -f label=cleanbuild --format '{{.ID}}' | xargs -r docker kill
```
