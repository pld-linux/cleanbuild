# cleanbuild vserver engine

cleanbuild vserver engine requires vserver with special customizations.

Dependencies:
- git-core
- mount
- perl-base
- poldek
- rpm-build-tools >= 4.5-3
- subversion
- sudo

# Setup

## Install all dependencies

configure passwordless sudo for user you use for cleanbuild

    visudo

add something like (assuming you have 'builder' as dedicated account)

    builder ALL=(ALL) NOPASSWD: ALL
    Defaults:builder env_keep += USER

create rpm dir

    builder --init-rpm-dir

create directories

    mkdir ~/rpm/cleanRPMS
    mkdir ~/rpm/cleanRPMS.repo

checkout the repo

    git clone ssh://git@git.pld-linux.org/projects/cleanbuild

set local user name

    echo 'USER="builder"' > .cleanbuildrc

Notes for vserver:

  If you are inside vserver, you need to have following context capabilities
  (add these lines to `/etc/vserver/<name>/ccapabilities`):

    SECURE_MOUNT
    BINARY_MOUNT

  And following system capabilities (add these lines to
  `/etc/vserver/<name>/bcapabilities`):

    MKNOD
    SYS_ADMIN
