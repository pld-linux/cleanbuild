#!/bin/sh
set -eu

PROGRAM=${0##*/}

# defaults
: ${PACKAGE_NAME=''}
: ${NETWORKING=false}
: ${TRACING=false}
: ${WITH=}
: ${WITHOUT=}
: ${KEEP_CONTAINER=true}
: ${TMPFS="4G"}
: ${EXEC=false}
: ${IGNORE_PACKAGES="systemd-init"}

dir=$(pwd)
image=registry.gitlab.com/pld-linux/cleanbuild
topdir=$dir/rpm
home=/home/builder

notice() {
	echo >&2 "[cleanbuild:notice]: $*"
}

die() {
	local rc=${2:-1}
	echo >&2 "[cleanbuild:error]: $1"
	exit $rc
}

is_no() {
	# Test syntax
	if [ $# = 0 ]; then
		return 2
	fi

	case "$1" in
	no|No|NO|false|False|FALSE|off|Off|OFF|N|n|0)
		# true returns zero
		return 0
		;;
	*)
		# false returns one
		return 1
		;;
	esac
}

is_bool() {
	[ "$1" = "true" -o "$1" = "false" ] || die "Invalid boolean value: $1"
}

tmpfs() {
	if is_no "${TMPFS:-true}" || [ "$TMPFS" = "0" ]; then
		return
	fi

	echo "--tmpfs $home/rpm/BUILD:rw,exec,nosuid,size=$TMPFS"
}

have_container() {
    local name="$1" id

    id=$(docker ps -f "label=cleanbuild=$name" --format '{{.ID}}')

    test -n "$id"
}

create_container() {
	if ! $KEEP_CONTAINER; then
		notice "Clean up old container: $name"
		docker kill $name >/dev/null 2>&1 || :
		docker rm $name >/dev/null 2>&1 || :
	fi

	install -d $topdir/logs

	# start the container
	if ! have_container "$PACKAGE_NAME"; then
		TMPFS_SIZE=$TMPFS \
		PACKAGE_NAME=$PACKAGE_NAME \
		docker-compose run --rm -d \
			--name=$name \
			--workdir=$home/rpm/packages/$PACKAGE_NAME \
			--label=cleanbuild=$PACKAGE_NAME \
			cleanbuild
	fi

	UID=$(id -u)
	GID=$(id -g)
	notice "Setup builder user ($UID:$GID)"

	docker exec --user=root -w / $name usermod -d $home builder

	if [ "$UID" -gt 0 ]; then
		docker exec --user=root -w / $name usermod -u $UID builder
	fi
	if [ "$GID" -gt 0 ]; then
		docker exec --user=root -w / $name groupmod -g $GID builder
	fi

	notice "Setup permissions"
	docker exec --user=root -w / $name sh -c "cd $home && chown builder:builder . rpm rpm/logs rpm/BUILD rpm/RPMS rpm/packages .ccache"

	if [ ! -d $topdir/rpm-build-tools ]; then
		notice "Initialize rpm-build-tools"
		docker exec -w / $name builder --init-rpm-dir
	fi
}

enter_container() {
	notice "Entering container for $PACKAGE_NAME"
	docker exec --user=root -it $name bash
}

generate_shell_code() {
	local shell="$1"

	case "$shell" in
	bash|ksh|zsh)
		echo "alias cleanbuild=$dir/cleanbuild"
		;;
	*)
		die "Unsupported shell: $shell"
		;;
	esac
}

package_prepare() {
	notice "Fetch sources and install dependencies"
	if [ -d $topdir/packages/$PACKAGE_NAME ]; then
		# chown, as it might be different owner (root) modified outside container
		notice "Fix ownership of existing package directory"
		docker exec --user=root -w / $name chown -R builder:builder $home/rpm/packages/$PACKAGE_NAME
	fi

	notice "Fetch package sources"
	docker exec --user=root -w / $name setfacl -x u:builder /etc/resolv.conf
	docker exec -w / $name builder -g $PACKAGE_NAME

	if ! $NETWORKING; then
		notice "Disable networking: Prevent network access for user builder like PLD Linux builders"
		docker exec --user=root -w / $name setfacl -m u:builder:--- /etc/resolv.conf
	fi

	notice "Find latest tag on the branch"
	git_tag=$(docker exec -w / -e GIT_DIR=$home/rpm/packages/$PACKAGE_NAME/.git $name git describe --tags --always)
	buildlog=rpm/logs/${git_tag#auto/*/}.log
	notice "Build log: $buildlog"
}

# Configure php versions, so that only one version is active
configure_php() {
	local PHP_VERSION=5.3
	local ignore_packages

	notice "Configure php: $PHP_VERSION"
	ignore_packages="$IGNORE_PACKAGES *php4* *php52* *php53* *php54* *php55* *php56* *php70* *php71* *php72* *php73* *php74* *php80* *php81* *php82*"
	ignore_packages=$(echo "$ignore_packages" | sed -e "s/ \*php${PHP_VERSION/./}\*//")

	docker exec --user=root -w / $name poldek-config ignore "$ignore_packages"
}

package_build() {
	# create default args for builder
	set -- -nn ${WITH:+--with "${WITH# }"} ${WITHOUT:+--without "${WITHOUT# }"} "$PACKAGE_NAME"

	configure_php

	while true; do
		notice "Install dependencies"
		docker exec -w / -t $name builder -g -R "$@"
		notice "Remove .la dependencies"
		docker exec --user=root -w / $name $home/cleanbuild/bin/cleanup-la
		notice "Reset findunusedbr state after deps install"
		docker exec --user=root -w / $name $home/cleanbuild/bin/findunusedbr -c / $home/rpm/packages/$PACKAGE_NAME/$PACKAGE_NAME.spec

		notice "Build package"
		docker exec -w $home $name cleanbuild/bin/teeboth $buildlog builder -bb --define '__spec_clean_body %{nil}' "$@" && rc=$? || rc=$?
		# Kill processes on Ctrl+C
		if [ "$rc" = 255 ]; then
			docker exec -w / $name pkill -e -u builder
			die "Aborted" $rc
		fi

		findbr=$PACKAGE_NAME.findbr.log
		builddir=$(docker exec -w $home $name sh -ec 'set -- rpm/BUILD/*; echo "$1"')
		if [ -z "$builddir" ]; then
			die "Unable to determine build dir. Build failed?" 6
		fi
		notice "Execute findbr"
		docker exec --user=root -w / $name sh -c "cd $home && cleanbuild/bin/findbr $builddir $buildlog" > $findbr

		installed_something=false
		while read pkg msg; do
			bin/addbr rpm/packages/$PACKAGE_NAME/$PACKAGE_NAME.spec "$pkg" "$msg" || continue
			installed_something=true
		done < $findbr
		rm -f $findbr

		# go for another try
		$installed_something && continue

		notice "Execute findunusedbr"
		docker exec --user=root -w / $name $home/cleanbuild/bin/findunusedbr -c / $home/rpm/packages/$PACKAGE_NAME/$PACKAGE_NAME.spec

		if [ $rc -eq 0 ] && ! $KEEP_CONTAINER; then
			notice "Finished ok, cleanup container"
			docker kill $name >/dev/null && docker rm $name >/dev/null || :
		fi

		# propagate error
		exit $rc
	done
}

parse_options() {
	local t
	t=$(getopt -o 'x' --long 'network,exec,no-tmpfs,notmpfs,tmpfs:,shellcode:,keep-container:,with:,without:' -n "$PROGRAM" -- "$@")
	[ $? != 0 ] && exit $?
	eval set -- "$t"

	while :; do
		case "$1" in
		-x)
			TRACING=true
			;;
		--network)
			NETWORKING=true
			;;
		--exec)
			EXEC=true
			;;
		--no-tmpfs|--notmpfs)
			TMPFS=false
			;;
		--shellcode)
			shift
			generate_shell_code "$1"
			exit 0
			;;
		--tmpfs)
			shift
			TMPFS="$1"
			;;
		--keep-container)
			shift
			is_bool "$1"
			KEEP_CONTAINER=$1
			;;
		--with)
			shift
			WITH="$WITH,$1"
			;;
		--without)
			shift
			WITHOUT="$WITHOUT,$1"
			;;
		--)
			shift
			break
			;;
		*)
			die "Internal error: [$1] not recognized!"
			;;
		esac
		shift
	done

	test "$#" -eq 1 || die "Package not specified or excess arguments"
	PACKAGE_NAME="${1%.spec}"
}

main() {
	parse_options "$@"

	$TRACING && set -x
	local name="cleanbuild-$PACKAGE_NAME"
	if $EXEC; then
		enter_container
		return
	fi
	create_container
	package_prepare
	package_build
}

main "$@"
