# cleanbuild

cleanbuild is tool to help discovering missing (or excessive) dependencies by
building packages in clean build environment.

cleanbuild comes with two backends:
- vserver (legacy)
- docker

See [vserver](README.vserver.md) or [docker](README.docker.md) for engine specific instructions.

Run cleanbuild:

    ./cleanbuild FHS

You can create yourself alias for easier invocation from regular user.
Add this to your `~/.bashrc`:

```bash
eval "$(/path-to-cleanbuild-checkout/cleanbuild --shellcode=bash)"
```

And then:
```bash
cleanbuild somepkg
```
