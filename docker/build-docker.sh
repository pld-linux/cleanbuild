#!/bin/sh
# Use this script to build the image locally
set -eu

image=registry.gitlab.com/pld-linux/cleanbuild

script=$(readlink -f "$0")
dir=$(dirname "$0")
cd "$dir"

export DOCKER_BUILDKIT=1
docker build --pull -t $image .
